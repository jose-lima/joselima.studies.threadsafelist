﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace joselima.studies.threadsafelist.console
{
    class Program
    {
        static void Main( string[] args )
        {
            const int nrWrites = 20;
            const int nrRemoves = 20;
            const int nrReads = 20;

            var safeList = new ThreadSafeList();

            // setup tasks
            var writeTasks = Enumerable.Range( 0, nrWrites )
                .Select( x => new Task( () => safeList.Add( new Item( x.ToString() ) ) ) )
                .ToList();

            var removeTasks = Enumerable.Range( 0, nrRemoves )
                .Select( x => new Task( () => safeList.Remove( x.ToString() ) ) )
                .ToList();

            var readTasks = Enumerable.Range( 0, nrReads )
                .Select( x => new Task( () => safeList.Get(x.ToString()) ))
                .ToList();


            // execute all tasks in parallel
            Console.WriteLine( "Writing, removing, and reading items in parallel simultaneously..." );
            Console.WriteLine();

            var allTasks = writeTasks.Concat( removeTasks ).Concat(readTasks).ToList();
            allTasks.ForEach( t => t.Start() );
            Task.WaitAll( allTasks.ToArray() );

            Console.WriteLine();
            Console.WriteLine( "Program finished..." );
            Console.ReadLine();
        }
    }

}
