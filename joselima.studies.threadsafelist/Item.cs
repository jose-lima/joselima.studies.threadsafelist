﻿namespace joselima.studies.threadsafelist
{
    public class Item
    {
        public Item( string id )
        {
            Id = id;
        }

        public string Id
        {
            get;
        }

        public override string ToString()
        {
            return $"{Id}";
        }
    }
}
