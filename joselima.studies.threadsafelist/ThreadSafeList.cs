﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace joselima.studies.threadsafelist
{
    public class ThreadSafeList
    {
        // main data
        private List<Item> _list = new List<Item>();

        // debug/diagnostics
        private static Stopwatch stopwatch = Stopwatch.StartNew();

        // state variables
        private int _activeReadingTasksCount;
        private bool _isEditing;

        // synchronization objects
        private object _stateAccessLock = new object();
        private SemaphoreSlim _editingSemaphore = new SemaphoreSlim( 1 );
        private ManualResetEventSlim _stateChanged = new ManualResetEventSlim(true );

        // real-world delay times simulation
        private const int READING_DELAY_MS = 0;
        private const int ADDING_DELAY_MS = 0;
        private const int REMOVING_DELAY_MS = 0;

        #region safe public CRUD methods

        public Item Get(string id)
        {
            // if (!editing) -> proceed
            // on proceed block edit but not read
            WaitForReading();

            Item foundItem;
            try
            {
                foundItem = UnsafeGet(id);
                var foundItemStr = foundItem != null ? $"found item '{foundItem}'" : $"nothing found for id '{id}'";
                Console.WriteLine($"{stopwatch.ElapsedMilliseconds}ms | \tGet: {foundItemStr}");
            }
            finally
            {
                // decrease reading tasks count
                ExitReading();
            }

            return foundItem;
        }

        public void Add(Item item)
        {
            SafeEdit( () => UnsafeAdd( item ) );
        }

        public void Remove( string id )
        {
            SafeEdit( () => UnsafeRemove( id ) );
        }

        #endregion


        #region base unsafe CRUD methods

        private Item UnsafeGet( string id )
        {
            Thread.Sleep( READING_DELAY_MS );
            var foundItem = _list.SingleOrDefault( x => x.Id.Equals( id ) );
            return foundItem;
        }

        private void UnsafeAdd( Item item )
        {
            Thread.Sleep( ADDING_DELAY_MS );
            _list.Add( item );
            Console.WriteLine( $"{stopwatch.ElapsedMilliseconds}ms | \tAdd: added item '{item}'" );
        }

        private void UnsafeRemove( string id )
        {
            Thread.Sleep( REMOVING_DELAY_MS );
            var match = UnsafeGet( id );
            if( match == null )
            {
                Console.WriteLine( $"{stopwatch.ElapsedMilliseconds}ms | \tRemove: nothing to remove with id '{id}'" );
                return;
            }

            _list.Remove( match );
            Console.WriteLine( $"{stopwatch.ElapsedMilliseconds}ms | \tRemove: removed item with id '{id}'" );
        }

        #endregion


        #region synchronization helper methods

        private void SafeEdit( Action editAction )
        {
            // if (editing | reading) - > block
            // on proceed block edit & read
            WaitForEditing();

            try
            {
                editAction.Invoke();
            }
            finally
            {
                ExitEditing();
            }
        }

        private void WaitForReading()
        {
            while( true )
            {
                _stateChanged.Wait();
                lock( _stateAccessLock )
                {
                    if( _isEditing ) // require no active editors but disregard readers
                    {
                        continue;
                    }

                    _activeReadingTasksCount++;
                    break;
                }
            }
        }

        private void WaitForEditing()
        {
            while( true )
            {
                _stateChanged.Wait(); // wait state update signal

                lock( _stateAccessLock ) // check/update state atomically
                {
                    if( _isEditing || _activeReadingTasksCount != 0 ) // require both no active editors or readers
                    {
                        continue;
                    }

                    _editingSemaphore.Wait(); // gain exclusive editing rights
                    _isEditing = true;
                    break;
                }
            }
        }

        private void ExitReading()
        {
            lock( _stateAccessLock ) // check/update state atomically
            {
                _activeReadingTasksCount--;
                _stateChanged.Set();
            }
        }

        private void ExitEditing()
        {
            // reset edit state
            lock( _stateAccessLock ) // check/update state atomically
            {
                _isEditing = false;
                _stateChanged.Set();
            }

            _editingSemaphore.Release(); // release exclusive editing rights
        }

        #endregion
    }
}
